package com.revature.controllers;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.catalina.servlets.DefaultServlet;
//import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.revature.beans.Reimbursement;
import com.revature.services.ERSService;

@WebServlet("/userServlet")
public class UserServlet extends DefaultServlet {
	private static final long serialVersionUID = -4657159152045991366L;
	private static ERSService service = new ERSService();
	// private static final Logger log = Logger.getRootLogger();

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Avoid CORS error
		response.addHeader("Access-Control-Allow-Origin", "*");

		// Delegate back to the parent service method for HTTP method delegation
		super.service(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		String body = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));

		ObjectMapper mapper = new ObjectMapper();
		Reimbursement reimbursement = mapper.readValue(body, Reimbursement.class);
		
		String userId = "0";
		try (BufferedReader br = new BufferedReader(new FileReader("out.txt"))) {
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while (line != null) {
				sb.append(line);
				sb.append(System.lineSeparator());
				line = br.readLine();
			}
			userId = sb.toString();
		}

		reimbursement.setAuthor(Integer.parseInt(userId.toString().trim()));
		reimbursement.setStatus("pending");
		reimbursement.setType(reimbursement.getType().toUpperCase());
		reimbursement.setResolver(Integer.parseInt(userId.toString().trim()));
		
		service.createReimbursement(reimbursement);
		
		response.setStatus(210);

		String jsonInString = mapper.writeValueAsString(userId);

		response.getWriter().write(jsonInString);
		response.getWriter().flush();
		response.getWriter().close();
	}
}
