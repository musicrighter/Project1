package com.revature.controllers;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.catalina.servlets.DefaultServlet;
import org.apache.log4j.Logger;

public class ErrorController extends DefaultServlet {
	private static final long serialVersionUID = -2927814833730982783L;
	private static final Logger log = Logger.getRootLogger();
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		Throwable throwable = (Throwable) request.getAttribute("javax.servlet.error.exception");
		
		int status_code = (Integer) request.getAttribute("javax.servlet.error.status_code");
		
		log.trace(throwable == null ? "No exception" : throwable.toString());
		PrintWriter writer = response.getWriter();
		writer.write("Sorry, that's a " + status_code);
		writer.close();
	}

}
