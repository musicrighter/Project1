package com.revature.controllers;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.catalina.servlets.DefaultServlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.revature.beans.Reimbursement;
import com.revature.beans.User;
import com.revature.services.ERSService;

@WebServlet("/loginServlet")
public class LoginServlet extends DefaultServlet {
	private static final long serialVersionUID = -8233489004903215512L;
	private static ERSService service = new ERSService();

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Avoid CORS error
		response.addHeader("Access-Control-Allow-Origin", "*");

		// Delegate back to the parent service method for HTTP method delegation
		super.service(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String body = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));

		ObjectMapper mapper = new ObjectMapper();
		User user = mapper.readValue(body, User.class);

		int check = service.checkCredentials(user.getUsername(), user.getPassword());
		if (check == -1) {
			response.setStatus(212);
		} else {
			try {
				// Create file
				FileWriter fstream = new FileWriter("out.txt");
				BufferedWriter out = new BufferedWriter(fstream);
				out.write(Integer.toString(check));
				// Close the output stream
				out.close();
			} catch (Exception e) {// Catch exception if any
				System.err.println("Error: " + e.getMessage());
			}

			User realUser = service.getUser(check);

			int role = realUser.getRole();

			if (role == 1) {
				List<Reimbursement> reimbursements = service.getReimbursements(check);
				response.setStatus(210);

				List<String> reimbursementStrings = new ArrayList<>();

				for (Reimbursement reimbursement : reimbursements) {
					reimbursementStrings.add(reimbursement.toString());
				}

				String jsonInString = mapper.writeValueAsString(reimbursementStrings);

				response.getWriter().write(jsonInString);
				response.getWriter().flush();
				response.getWriter().close();
			} else if (role == 2) {
				List<Reimbursement> reimbursements = service.viewReimbursements(check);
				response.setStatus(211);

				List<String> reimbursementStrings = new ArrayList<>();

				for (Reimbursement reimbursement : reimbursements) {
					reimbursementStrings.add(reimbursement.toString());
				}

				String jsonInString = mapper.writeValueAsString(reimbursementStrings);

				response.getWriter().write(jsonInString);
				response.getWriter().flush();
				response.getWriter().close();
			}
		}
	}
}