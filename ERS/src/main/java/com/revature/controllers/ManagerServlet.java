package com.revature.controllers;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.catalina.servlets.DefaultServlet;
//import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.revature.beans.Reimbursement;
import com.revature.services.ERSService;

@WebServlet("/managerServlet")
public class ManagerServlet extends DefaultServlet {
	private static final long serialVersionUID = -4657159152045991366L;
	private static ERSService service = new ERSService();
	// private static final Logger log = Logger.getRootLogger();

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Avoid CORS error
		response.addHeader("Access-Control-Allow-Origin", "*");

		// Delegate back to the parent service method for HTTP method delegation
		super.service(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		String body = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));

		String userId = "0";
		try (BufferedReader br = new BufferedReader(new FileReader("out.txt"))) {
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while (line != null) {
				sb.append(line);
				sb.append(System.lineSeparator());
				line = br.readLine();
			}
			userId = sb.toString();
		}

		ObjectMapper mapper = new ObjectMapper();
		Reimbursement reimbursement = mapper.readValue(body, Reimbursement.class);

		int status = Integer.parseInt(reimbursement.getStatus());
		int id = reimbursement.getId();

		service.updateReimbursement(Integer.parseInt(userId.toString().trim()), id, status);
		
		response.setStatus(210);

	}
}