package com.revature.beans;

import java.sql.Timestamp;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class Reimbursement {
	private int id;
	private double amount;
	private Timestamp submitted;
	private Timestamp resolved;
	private String description;
	private Object receipt;
	private int author;
	private int resolver;
	private String status;
	private String type;
	private static NumberFormat currencyFormat = NumberFormat.getCurrencyInstance(Locale.US);

	public Reimbursement() {

	}

	public Reimbursement(int id, double amount, Timestamp submitted, Timestamp resolved, String description,
			Object receipt, int author, int resolver, String status, String type) {
		super();
		this.id = id;
		this.amount = amount;
		this.submitted = submitted;
		this.resolved = resolved;
		this.description = description;
		this.receipt = receipt;
		this.author = author;
		this.resolver = resolver;
		this.status = status;
		this.type = type;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public Timestamp getSubmitted() {
		return submitted;
	}

	public void setSubmitted(Timestamp submitted) {
		this.submitted = submitted;
	}

	public Timestamp getResolved() {
		return resolved;
	}

	public void setResolved(Timestamp resolved) {
		this.resolved = resolved;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Object getReceipt() {
		return receipt;
	}

	public void setReceipt(Object receipt) {
		this.receipt = receipt;
	}

	public int getAuthor() {
		return author;
	}

	public void setAuthor(int author) {
		this.author = author;
	}

	public int getResolver() {
		return resolver;
	}

	public void setResolver(int resolver) {
		this.resolver = resolver;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public static NumberFormat getCurrencyFormat() {
		return currencyFormat;
	}

	public static void setCurrencyFormat(NumberFormat currencyFormat) {
		Reimbursement.currencyFormat = currencyFormat;
	}

	@Override
	public String toString() {
		String sub = null;
		String res = null;
		if (submitted != null) {
			sub = new SimpleDateFormat("MM/dd/yyyy").format(submitted);
		}
		if (resolved != null) {
			res = new SimpleDateFormat("MM/dd/yyyy").format(resolved);
		}
		
		return "Amount: " + currencyFormat.format(amount) + ", submitted: " + sub + ", resolved: " + res
				+ ", description: " + description + ", receipt: " + receipt + ", author: " + author + ", resolver: "
				+ resolver + ", status: " + status + ", type: " + type.toLowerCase() + ", (" + id + ")";
	}

}
