package com.revature.daos;

import java.util.List;

import com.revature.beans.Reimbursement;
import com.revature.beans.User;

public interface ERSDao {
	public List<Reimbursement> getReimbursements(int id);

	public List<Reimbursement> viewReimbursements(int id);

	public int createReimbursement(Reimbursement reimbursement);

	public int updateReimbursement(int userId, int reimbId, int status);

	public int checkCredentials(String user, String pass);

	public int createUser(User user);
	
	public User getUser(int id);
}
