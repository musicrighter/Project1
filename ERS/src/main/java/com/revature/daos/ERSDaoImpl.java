package com.revature.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import com.revature.beans.User;
import com.revature.beans.Reimbursement;
import com.revature.util.ConnectionUtil;

public class ERSDaoImpl implements ERSDao {

	private ConnectionUtil connectionUtil = ConnectionUtil.getConnectionUtil();

	private Reimbursement extractReimbursements(ResultSet results) throws SQLException {
		Reimbursement reimbursement = new Reimbursement();
		reimbursement.setId(results.getInt("reimb_id"));
		reimbursement.setAmount(results.getDouble("reimb_amount"));
		reimbursement.setSubmitted(results.getTimestamp("reimb_submitted"));
		reimbursement.setResolved(results.getTimestamp("reimb_resolved"));
		reimbursement.setDescription(results.getString("reimb_description"));
		reimbursement.setReceipt(results.getString("reimb_receipt"));
		reimbursement.setAuthor(results.getInt("reimb_author"));
		reimbursement.setResolver(results.getInt("reimb_resolver"));
		reimbursement.setStatus(results.getString("reimb_status"));
		reimbursement.setType(results.getString("reimb_type"));
		return reimbursement;
	}
	
	private User extractUser(ResultSet results) throws SQLException {
		User user = new User();
		user.setId(results.getInt("ers_users_id"));
		user.setUsername(results.getString("ers_username"));
		user.setPassword(results.getString("ers_password"));
		user.setFirstName(results.getString("user_first_name"));
		user.setLastName(results.getString("user_last_name"));
		user.setEmail(results.getString("user_email"));
		user.setRole(results.getInt("user_role_id"));
		return user;
	}

	@Override
	public int checkCredentials(String user, String pass) {
		try (Connection conn = connectionUtil.getConnection()) {
			String sql = "SELECT ers_users_id, ers_username, ers_password FROM ers_users WHERE ers_username = ? AND ers_password = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, user);
			ps.setString(2, pass);
			ResultSet results = ps.executeQuery();

			if (results.next()) {
				return results.getInt("ers_users_id");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}

	@Override
	public int createUser(User user) {
		try (Connection conn = connectionUtil.getConnection()) {
			String sql = "INSERT INTO ers_users \r\n"
					+ "(ers_username, ers_password, user_first_name, user_last_name, user_email, user_role_id) "
					+ "values (?, ?, ?, ?, ?, ?) RETURNING ers_users_id";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, user.getUsername());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getFirstName());
			ps.setString(4, user.getLastName());
			ps.setString(5, user.getEmail());
			ps.setInt(6, user.getRole());
			ResultSet results = ps.executeQuery();

			if (results.next()) {
				user.setId(results.getInt("ers_users_id"));
				return results.getInt("ers_users_id");
			}

		} catch (SQLException e) {
			return -1;
		}
		return -1;
	}

	@Override
	public List<Reimbursement> getReimbursements(int id) {
		try (Connection conn = connectionUtil.getConnection()) {
			String sql = "SELECT * FROM ers_reimbursement NATURAL JOIN ers_reimbursement_status NATURAL JOIN ers_reimbursement_type WHERE reimb_author = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet results = ps.executeQuery();
			List<Reimbursement> reimbursements = new ArrayList<>();
			while (results.next()) {
				reimbursements.add(extractReimbursements(results));
			}
			return reimbursements;
		} catch (SQLException e) {
			return null;
		}
	}

	@Override
	public List<Reimbursement> viewReimbursements(int id) {
		try (Connection conn = connectionUtil.getConnection()) {
			String sql = "SELECT * FROM ers_reimbursement NATURAL JOIN ers_reimbursement_status NATURAL JOIN ers_reimbursement_type WHERE reimb_author != ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet results = ps.executeQuery();
			List<Reimbursement> reimbursements = new ArrayList<>();
			while (results.next()) {
				reimbursements.add(extractReimbursements(results));
			}
			return reimbursements;
		} catch (SQLException e) {
			return null;
		}
	}

	@Override
	public int createReimbursement(Reimbursement reimbursement) {
		try (Connection conn = connectionUtil.getConnection()) {
			String sql = "INSERT INTO ers_reimbursement "
					+ "(reimb_amount, reimb_submitted, reimb_resolved, reimb_description, reimb_receipt, reimb_author, reimb_resolver, reimb_status_id, reimb_type_id) "
					+ "values (?, now(), ?, ?, ?, ?, ?, "
					+ "	(select reimb_status_id from ers_reimbursement_status where reimb_status = ?), "
					+ "	(select reimb_type_id from ers_reimbursement_type where reimb_type = ?)) RETURNING reimb_id";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setDouble(1, reimbursement.getAmount());
//			ps.setTimestamp(2, reimbursement.getSubmitted());
			ps.setTimestamp(2, reimbursement.getResolved());
			ps.setString(3, reimbursement.getDescription());
			ps.setString(4, reimbursement.getReceipt().toString());
			ps.setInt(5, reimbursement.getAuthor());
			ps.setInt(6, reimbursement.getResolver());
			ps.setString(7, reimbursement.getStatus());
			ps.setString(8, reimbursement.getType());
			ResultSet results = ps.executeQuery();
			if (results.next()) {
				reimbursement.setId(results.getInt("reimb_id"));
				return results.getInt("reimb_id");
			}
		} catch (SQLException e) {
			System.out.println(e);
		}
		return -1;
	}

	@Override
	public int updateReimbursement(int userId, int reimbId, int status) {
		try (Connection conn = connectionUtil.getConnection()) {
			String sql = "UPDATE ers_reimbursement SET reimb_resolved = now(), reimb_status_id = ?, reimb_resolver = ? WHERE reimb_id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, status);
			ps.setInt(2, userId);
			ps.setInt(3, reimbId);
			ResultSet results = ps.executeQuery();
			if (results.next()) {
				return results.getInt("reim_id");
			}
		} catch (SQLException e) {
			return -1;
		}
		return -1;
	}

	@Override
	public User getUser(int id) {
		try (Connection conn = connectionUtil.getConnection()) {
			String sql = "select * from ers_users where ers_users_id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet results = ps.executeQuery();
			if (results.next()) {
				return extractUser(results);
			}
		} catch (SQLException e) {
			return null;
		}
		return null;
	}
}