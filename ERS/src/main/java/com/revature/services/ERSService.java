package com.revature.services;

import java.util.List;

import com.revature.beans.User;
import com.revature.daos.ERSDaoImpl;
import com.revature.daos.ERSDao;
import com.revature.beans.Reimbursement;

public class ERSService {

	public ERSDao dao = new ERSDaoImpl();

	public List<Reimbursement> getReimbursements(int id) {
		return dao.getReimbursements(id);
	}

	public List<Reimbursement> viewReimbursements(int id) {
		return dao.viewReimbursements(id);
	}

	public int createReimbursement(Reimbursement reimbursement) {
		return dao.createReimbursement(reimbursement);
	}

	public int updateReimbursement(int userId, int reimbId, int status) {
		return dao.updateReimbursement(userId, reimbId, status);
	}

	public int checkCredentials(String user, String pass) {
		return dao.checkCredentials(user, pass);
	}

	public int createUser(User user) {
		return dao.createUser(user);
	}
	
	public User getUser(int id) {
		return dao.getUser(id);
	}
}
